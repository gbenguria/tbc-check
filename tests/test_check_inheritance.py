from copy import deepcopy

import pytest

from tbc_check import checker

TPL_MULTIPLE_JOB = {
        "workflow": None,
        "variables": None,
        "stages": None,
        ".base-job": { "image": "base-image" },
        "prefix-job1": {
            "stage": "build",
            "extends": ".base-job",
        },
        "prefix-job2": {
            "stage": "test",
            "extends": ".base-job"
        },
    }

TPL_SINGLE_JOB = {
        "workflow": None,
        "variables": None,
        "stages": None,
        "prefix-job1": {
            "stage": "build",
        },
    }

def test_check_inheritance_multiple_job_inheritance_from_undefined(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_MULTIPLE_JOB)
    tpl_body["prefix-job2"]["extends"] = ".undefined"

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;32m✓\x1b[0m job '.base-job' inheritance: OK\n"
        "  \x1b[0;32m✓\x1b[0m job 'prefix-job1' inheritance: OK\n"
        "  \x1b[0;33m⚠ job 'prefix-job2' inherits from '.undefined' which is not defined in current template\x1b[0m\n"
    )

def test_check_inheritance_multiple_job_inheritance_bad(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_MULTIPLE_JOB)
    tpl_body["prefix-job2"].pop("extends")

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;32m✓\x1b[0m job '.base-job' inheritance: OK\n"
        "  \x1b[0;32m✓\x1b[0m job 'prefix-job1' inheritance: OK\n"
        "  \x1b[0;31m✕ multiple job template: non-hidden job 'prefix-job2' should inherit a base job\x1b[0m\n"
    )

def test_check_inheritance_multiple_job_inheritance_good(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_MULTIPLE_JOB)

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;32m✓\x1b[0m job '.base-job' inheritance: OK\n"
        "  \x1b[0;32m✓\x1b[0m job 'prefix-job1' inheritance: OK\n"
        "  \x1b[0;32m✓\x1b[0m job 'prefix-job2' inheritance: OK\n"
    )


def test_check_inheritance_multiple_job_inheritance_from_non_hidden(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_MULTIPLE_JOB)
    tpl_body["prefix-job2"]["extends"] = "prefix-job1"

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;32m✓\x1b[0m job '.base-job' inheritance: OK\n"
        "  \x1b[0;32m✓\x1b[0m job 'prefix-job1' inheritance: OK\n"
        "  \x1b[0;31m✕ job 'prefix-job2' inherits from non-hidden job 'prefix-job1'\x1b[0m\n"
    )


def test_check_inheritance_single_job_without_inheritance(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_SINGLE_JOB)

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;32m✓\x1b[0m job 'prefix-job1' inheritance: OK\n"
    )

def test_check_inheritance_single_job_with_inheritance(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_SINGLE_JOB)
    tpl_body["prefix-job1"]["extends"] = ".base-job"

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;33m⚠ single job template: job 'prefix-job1' inherits another job\x1b[0m\n"
        "  \x1b[0;33m⚠ job 'prefix-job1' inherits from '.base-job' which is not defined in current template\x1b[0m\n"
    )

def test_check_inheritance_single_job_override_only(capfd: pytest.CaptureFixture[str]):
    tpl_body = deepcopy(TPL_SINGLE_JOB)
    tpl_body["prefix-job1"] = { "rules": {} }

    res = checker._check_inheritance(tpl_body=tpl_body)
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[90m✕ job 'prefix-job1' does not defines an actual job: skip\x1b[0m\n"
    )
